//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>

void factorial (int intvalue);

int main (int argc, const char* argv[])
{
    //variable to store user enter int
    int val;
    //string to print out
    std::cout << "Enter in a positive integer\n";
    //input of value
    std::cin >> val;
    //call to factorial function
    factorial(val);

    return 0;
}


void factorial (int intvalue)
{
    //initialised factorial number
    int fact = 1;
    //if the integer value is positive and not 0
    if (intvalue >= 1)
    {
    //for counter is 1, less than enter value
    for (int counter = 1; counter <= intvalue; counter ++) {
        //update factorial variable with each concurrent loop
        fact = fact * counter;
        //add value of counter to integer value string and end the line of text
        std:: cout << "integer value " << counter << ": result " << fact << std::endl;
    }
    //factorial of the entered number is printed out
    std:: cout << "factoral value = " << fact << std::endl;
    }
    
    // if the value is not a positive integer then alert user and stop running
    else {
        std::cout << "value is not a positive integer\n";
    }
    
}